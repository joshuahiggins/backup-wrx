'use strict';

angular.module('wrx', [
  'ngResource',
  'nvd3'
])

.factory('jsonSrvc', ['$resource', function ($resource) {
  return $resource('data.json',{ }, {
    getData: { method:'GET', isArray: false }
  });
}])

.controller('wrxCtrl', ['$scope', '$window', 'jsonSrvc', function ($scope, $window, jsonSrvc) {

  jsonSrvc.getData(function (data) {
    $scope.boostData = data.boost;
  });

  $scope.boostOpt = {
    chart: {
      type: 'lineChart',
      height: 650,
      width: 675,
      margin : {
          top: 20,
          right: 20,
          bottom: 60,
          left: 80
      },

      x: function(d){ return d[0]; },
      xAxis: {
        axisLabel: "Rotations Per Minute (RPM)"
      },
      forceX: [ 2450, 6550 ],
      
      y: function(d){ return d[1]; },
      yAxis: {
        axisLabel: "Boost Pressure in PSI"
      },
      forceY: [ -9, 17 ],

      interpolate: 'basis',
      transitionDuration: 300,
      useInteractiveGuideline: true,
      color: [
        "#0011FF",
        "#0055FF",
        "#0077FF",
        "#0099FF",
        "#00AAFF",
        "#FF1100",
        "#FF5500",
        "#FF7700",
        "#FF9900",
        "#FFAA00"
      ],
      clipVoronoi: false      
    }
  }
    
}]);