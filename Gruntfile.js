'use strict';

module.exports = function (grunt) {

  // Configure grunt
  grunt.initConfig({
    connect: {
      server: {
        options: {
          hostname: 'localhost',
          port: 9999,
          keepalive: true
        }
      }
    }
  });

  // Load all custom tasks
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', [ 'connect' ]);

};
